package chapter4.scheduler;

import chapter4.service.filmService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class filmScheduler extends QuartzJobBean {
    int filmID;
    public filmScheduler(int filmID) {
        this.filmID = filmID;
    }
    @Autowired
    public filmService filmService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        filmService.updateStatus(filmID);
    }
}
